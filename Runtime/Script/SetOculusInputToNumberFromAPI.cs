﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetOculusInputToNumberFromAPI : MonoBehaviour
{

    public OculusQuestToBigNumber m_input;
    public Transform m_oculusRootReference;
    public Transform m_centerEyeReference;
    public Transform m_leftHandReference;
    public Transform m_rightHandReference;

    void Update()
    {
        RefreshInput();
    }

    public void RefreshInput() {
          m_input.m_oculusInput.SetButtonA(OVRInput.Get(OVRInput.Button.One));
          m_input.m_oculusInput.SetButtonB(OVRInput.Get(OVRInput.Button.Two));
          m_input.m_oculusInput.SetButtonX(OVRInput.Get(OVRInput.Button.Three));
          m_input.m_oculusInput.SetButtonY(OVRInput.Get(OVRInput.Button.Four));
          m_input.m_oculusInput.SetButtonCancel(OVRInput.Get(OVRInput.Button.Back));
          m_input.m_oculusInput.SetButtonLeftJoystick (OVRInput.Get(OVRInput.Button.PrimaryThumbstick));
          m_input.m_oculusInput.SetButtonRightJoystick(OVRInput.Get(OVRInput.Button.SecondaryThumbstick));
              
          m_input.m_oculusInput. SetLeftJoystick (OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick));
          m_input.m_oculusInput. SetRightJoystick(OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick));
          m_input.m_oculusInput. SetTriggerLeft  (OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger));
          m_input.m_oculusInput. SetTriggerRight (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger));
          m_input.m_oculusInput. SetGrabLeft     (OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger));
          m_input.m_oculusInput. SetGrabRight    (OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger));
          m_input.m_oculusInput. SetLeftLocalPosition  (m_leftHandReference.position);
          m_input.m_oculusInput. SetRightLocalPosition (m_rightHandReference.position);
          m_input.m_oculusInput. SetLeftLocalRotation  (m_leftHandReference.rotation);
          m_input.m_oculusInput. SetRightLocalRotation (m_rightHandReference.rotation);
          m_input.m_oculusInput. SetHeadLocalPosition  (m_centerEyeReference.position);
          m_input.m_oculusInput. SetHeadLocalRotation  (m_centerEyeReference.rotation);
    }
}
